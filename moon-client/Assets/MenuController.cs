﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MenuController : MonoBehaviour {
    public static MenuController instance;

    public GameObject PlayerNameInput;

    private void Awake()
    {
        instance = this;
        gameObject.SetActive(false);
    }

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void TogglePanel()
    {
        gameObject.SetActive(!gameObject.activeSelf);
    }

    public void OpenPanel()
    {
        gameObject.SetActive(true);
    }

    public void ClosePanel()
    {
        gameObject.SetActive(false);
    }

    public void spawnPlayer()
    {
        //PlayerSpawnController.Instance.RequestCreatePlayer(PlayerNameInput.GetComponent<Text>().text);
    }
}
