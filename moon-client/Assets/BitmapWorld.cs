﻿//using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using System;
using System.Collections;

namespace HoneyFramework
{
    /*
     * Class which manages all world content. Its not designed to be instead of gameplay classes but rather be best point for all information about terrain, world settings and status
     */
    public class BitmapWorld : World
    {
		public Texture2D mapTexture;
		public int chunksWidth;
		public int chunksHeight;

        /// <summary>
        /// Initialization of basic data. This function calls world planning and related data loading and creation
        /// </summary>
        /// <returns></returns>
        public void InitializeFromBitmap()
        {
            status = Status.Preparation;

            TerrainDefinition.ReloadDefinitions();

			hexes = new Dictionary<Vector3i, Hex>();
			List<TerrainDefinition> tdList = TerrainDefinition.definitions.FindAll(o => o.source.mode == MHTerrain.Mode.normal);
			new HoneyBitmap (hexes, tdList, instance).ImportHexes(mapTexture);

            MyRiverFactory.CreateRivers(this, 50);

			//for (int y = 0; y < mapTexture.height; y++) {
			//	var coord = HoneyBitmap.CartesianToAxial (new Vector2i (8, y));
			//	Roads.SetRoad(coord, true);
			//}

            
			BuildChunks ();

            status = Status.TerrainGeneration;
        }

		public void BuildChunks() {
			status = Status.Preparation;
			for (int y = 0; y <= chunksHeight; y++) {
				for (int x = 0; x <= chunksWidth; x++) {
					PrepareChunkData (new Vector2i (x, y));
				}
			}
			status = Status.Finishing;
		}
	}
}