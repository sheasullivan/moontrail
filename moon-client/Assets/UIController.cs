﻿using UnityEngine;
using System.Collections;
using HoneyFramework;

public class UIController : MonoBehaviour {

    public static UIController instance;
    
    private void Awake()
    {
        instance = this;
    }

     // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void ToggleStatusPanel()
    {
        StatusController.instance.TogglePanel();
    }

    public void ToggleMenuPanel()
    {
        MenuController.instance.TogglePanel();
    }

    public void ToggleMoving()
    {
        var pc = GameManager.instance.currentPlayer.GetComponent<PlayerController>();
        pc.RequestToggleTraveling();
    }
}
