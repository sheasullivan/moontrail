﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using HoneyFramework;

public class HoneyBitmap {
	private static readonly float Threshold = .5f;
	private SortedDictionary<float, int> TerrainMapping = new SortedDictionary<float, int>();
	private Dictionary<Vector3i, Hex> hexes;
	private List<TerrainDefinition> tdList;
	private World instance;

	public HoneyBitmap(Dictionary<Vector3i, Hex> hexes, List<TerrainDefinition> tdList, World instance) {
		this.hexes = hexes;
		this.tdList = tdList;
		this.instance = instance;

		TerrainMapping.Add (.1f, 7); //water
		TerrainMapping.Add (.2f, 5); //marsh
		TerrainMapping.Add (.4f, 4); //grass
		TerrainMapping.Add (.6f, 3); //forest
		TerrainMapping.Add (.7f, 6); //high forest
		TerrainMapping.Add (.8f, 0); //dirt
		TerrainMapping.Add (.9f, 2); //hill
		TerrainMapping.Add (.95f, 1); //mountain
	}

	public void ImportHexes(Texture2D tex) {
		for (int x = 0; x < tex.width; x++) {
		//for (int y = 0; y < 16; y++) {
			for (int y = 0; y < tex.height; y++) {
			//for (int x = 0; x < 16; x++) {
				var level = tex.GetPixel(x, y).grayscale;
				var alpha = tex.GetPixel(x, y).a;

				if ( alpha > Threshold) {
					var position = CartesianToAxial(new Vector2i(x, y));
					hexes.Add(position, GetHexFromLevel(level, position, tdList, instance));
				}
			}
		}
	}

	public static Vector3i CartesianToAxial(Vector2i coord) {
		var axial = new Vector3i ();
		axial.y = coord.y - (int)(coord.x / 2);
		axial.x = coord.x;
		axial.z = (axial.x + axial.y) * -1;
		return axial;
	}

	private Hex GetHexFromLevel(float level, Vector3i position, List<TerrainDefinition> tdList, World instance) {
		var hex = new Hex ();
		//int index = (int)Mathf.Clamp((level * tdList.Count), 0, tdList.Count - 1);
		int index = tdList.Count - 1;

		foreach (var kvp in TerrainMapping) {
			if (level <= kvp.Key) {
				index = kvp.Value;
				break;
			}
		}

		hex.orderPosition = UnityEngine.Random.Range(0.0f, 1.0f);
		hex.rotationAngle = UnityEngine.Random.Range(0.0f, 360.0f);
		hex.terrainType = tdList[index];
		hex.position = position;
		
		instance.ReadyToPolishHex(hex);
		
		return hex;
	}
}
