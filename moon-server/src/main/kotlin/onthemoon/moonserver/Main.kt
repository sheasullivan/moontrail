package onthemoon.moonserver

import co.paralleluniverse.actors.behaviors.SupervisorActor

fun main(args: Array<String>) {
    val sup = SupervisorActor(SupervisorActor.RestartStrategy.ESCALATE).spawn()
    val factory = PlayerFactory(sup)
    val p = factory.Create("p1")
    val p2 = factory.Create("p2")

    p.send(Travel(p))
    p2.send(Travel(p2))

    //Thread.sleep(10000)
}
