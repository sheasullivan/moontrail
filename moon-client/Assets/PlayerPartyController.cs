﻿using UnityEngine;
using System.Collections;
using System;
using HoneyFramework;
using Stateless;
using Pathfinding;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine.UI;
//using BeardedManStudios.Network;

public class PlayerPartyController : NetworkBehaviour
{
    [SyncVar(hook = "OnHexPositionChanged")]
    public Vector3i HexPosition;

    [SyncVar]
    public float MoveProgress = 0;

    [SyncVar]
    public string PlayerID;

    public GameObject PlayerNameLabel;

    private StateMachine<PartyState, PartyTrigger> stateMachine;
    private float speed = .1f;

    private enum PartyState
    {
        Resting,
        Traveling
    }

    private enum PartyTrigger
    {
        Travel,
        Rest,
        ToggleTraveling
    }


    #region Shared
    // ********************************
    // Client and Server shared methods
    // ********************************

    void Awake()
    {
        InitStateMachine();
    }

    void Start()
    {
        transform.parent = World.instance.transform;
        transform.localPosition = Vector3.zero;
    }

    void Update()
    {
        if (isServer)
            UpdateServer();
        else if (isClient)
            UpdateClient();
    }

    protected void Rest()
    {
        stateMachine.Fire(PartyTrigger.Rest);
    }

    protected void Travel()
    {
        stateMachine.Fire(PartyTrigger.Travel);
    }

    protected void AnimateToPosition(Vector3i hexPosition)
    {
        var formation = GetComponent<Formation>();

        // Animate movement if distance is small, otherwise teleport
        if (HexCoordinates.HexDistance(formation.position, hexPosition) < 5)
        {
            formation.GoTo(hexPosition);
        }
        else
        {
            formation.TeleportTo(hexPosition);
        }
    }

    protected void UpdateFormation()
    {
        var formation = GetComponent<Formation>();
        AnimateToPosition(HexPosition);

        //TODO: Should be based on current status
        for (int i = 0; i < 5; i++)
        {
            formation.AddCharacter(CharacterActor.CreateCharacter("Characters/explorer", 2f));
        }
    }

    private void InitStateMachine()
    {
        stateMachine = new StateMachine<PartyState, PartyTrigger>(PartyState.Resting);
        stateMachine.Configure(PartyState.Resting)
            .OnEntry(t => StartResting())
            .OnExit(t => StopResting())
            .Permit(PartyTrigger.Travel, PartyState.Traveling)
            .Permit(PartyTrigger.ToggleTraveling, PartyState.Traveling);
        stateMachine.Configure(PartyState.Traveling)
            .OnEntry(t => StartTraveling())
            .OnExit(t => StopTraveling())
            .Permit(PartyTrigger.Rest, PartyState.Resting)
            .Permit(PartyTrigger.ToggleTraveling, PartyState.Resting);
    }

    private void OnHexPositionChanged(Vector3i value)
    {
        AnimateToPosition(value);
    }

    private void StartResting()
    {

    }

    private void StopResting()
    {

    }

    private void StartTraveling()
    {

    }

    private void StopTraveling()
    {

    }

    #endregion

    #region Client
    // *******************
    // Client only methods
    // *******************

    public override void OnStartClient()
    {
        base.OnStartClient();

        if (PlayerID == PlayerController.CurrentPlayer.PlayerID)
        {
            PlayerController.CurrentPlayer.Party = gameObject;
        }

        UpdateFormation();
    }

    [Client]
    private void UpdateClient()
    {
        DoMovementClient();
    }

    [Client]
    private void DoMovementClient()
    {
        if (stateMachine.IsInState(PartyState.Traveling))
        {
            MoveProgress += Time.deltaTime * speed;

            if (MoveProgress >= 1)
            {
                MoveProgress = 1;
            }
        }
    }

    #endregion

    #region Server
    // *******************
    // Server only methods
    // *******************

    public override void OnStartServer()
    {
        base.OnStartServer();
        //HexPosition = GameManager.trailStartPosition;
    }

    [Server]
    private void UpdateServer()
    {
        DoMovementServer();
    }

    [Server]
    private void DoMovementServer()
    {
        if (stateMachine.IsInState(PartyState.Traveling))
        {
            MoveProgress += Time.deltaTime * speed;

            if (MoveProgress >= 1)
            {
                MoveProgress = 0;
                AdvancePosition();
            }
        }
    }

    [Server]
    private void AdvancePosition()
    {
        GetComponent<Seeker>().StartPath(HexPosition, (Vector3)GameManager.trailEndPosition, MoveOneTile);
    }

    [Server]
    private void MoveOneTile(Path p)
    {
        var newPosition = new Vector3i(p.path[1].position);
        HexPosition = newPosition;
    }

    #endregion
}