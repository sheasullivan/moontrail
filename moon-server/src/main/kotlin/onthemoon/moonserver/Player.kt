package onthemoon.moonserver

import co.paralleluniverse.actors.*
import co.paralleluniverse.fibers.Suspendable
import co.paralleluniverse.kotlin.Actor
import co.paralleluniverse.kotlin.*
import co.paralleluniverse.strands.Strand
import com.github.oxo42.stateless4j.*
import com.github.oxo42.stateless4j.delegates.Action
import com.rabbitmq.client.Connection
import com.rabbitmq.client.Channel
import com.rabbitmq.client.ConnectionFactory
import onthemoon.pb.messaging.MessagingProtos

enum class PlayerState {RESTING, TRAVELING}
enum class PlayerTrigger {REST, TRAVEL}

data class Rest(val sender: ActorRef<Any?>)
data class Travel(val sender: ActorRef<Any?>)

class Player(val playerID: String) : Actor() {
    val stateMachineConfig: StateMachineConfig<PlayerState, PlayerTrigger> = StateMachineConfig()
    val stateMachine: StateMachine<PlayerState, PlayerTrigger>
    val rabbitChannel:Channel

    var moving = false
    var moveProgress = 0f
    var position = 0

    init {
        stateMachineConfig.configure(PlayerState.RESTING)
            .onEntry(Action { onStartResting() })
            .onExit(Action { onStopResting() })
            .permit(PlayerTrigger.TRAVEL, PlayerState.TRAVELING)
            .ignore(PlayerTrigger.REST)

        stateMachineConfig.configure(PlayerState.TRAVELING)
            .onEntry(Action { onStartTraveling() })
            .onExit(Action { onStopTraveling() })
            .permit(PlayerTrigger.REST, PlayerState.RESTING)
            .ignore(PlayerTrigger.TRAVEL)

        stateMachine = StateMachine(PlayerState.RESTING, stateMachineConfig)

        val factory = ConnectionFactory()
        //factory.setUsername(userName)
        //factory.setPassword(password)
        //factory.setVirtualHost(virtualHost)
        //factory.setHost(hostName)
        //factory.setPort(portNumber)
        val conn = factory.newConnection()
        rabbitChannel = conn.createChannel()
        rabbitChannel.exchangeDeclare("players-downstream", "direct", false)
    }

    @Suspendable
    override fun doRun() {
        var lastTime = System.currentTimeMillis()
        while(true) {
            val currentTime = System.currentTimeMillis()
            val deltaTime = currentTime - lastTime
            lastTime = currentTime

            onUpdate(deltaTime)
            checkMailbox()
            Strand.sleep(100)
        }
    }

    @Suspendable
    private fun checkMailbox() {
        tryReceive {
            when (it) {
                is Rest -> stateMachine.fire(PlayerTrigger.REST)
                is Travel -> stateMachine.fire(PlayerTrigger.TRAVEL)
                else -> null
            }
        }
    }

    private fun onUpdate(deltaTime: Long) {
        doMovement(2f, deltaTime)
    }

    private fun onStartResting() {
        println("start resting")
    }

    private fun onStopResting() {
        println("stop resting")
    }

    private fun onStartTraveling() {
        println("start traveling")

        moving = true
    }

    private fun onStopTraveling() {
        println("stop traveling")

        moving = false
    }

    private fun doMovement(speed: Float, deltaTime: Long) {
        if (moving) {
            moveProgress += (deltaTime / 1000f) * speed
            if (moveProgress >= 1) {
                position++
                println(playerID + ": " + position)
                moveProgress = 0f
            }
            println(playerID + ": " + moveProgress)
            val message = MessagingProtos.Player.newBuilder()
                    .setId(playerID)
                    .setMoveProgress(moveProgress)
                    .build()
            rabbitChannel.basicPublish("players-downstream", playerID, null, message.toByteArray())
        }
    }
}