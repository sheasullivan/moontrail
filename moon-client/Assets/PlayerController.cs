﻿using UnityEngine;
using System.Collections;
using System;
using HoneyFramework;
using Stateless;
using Pathfinding;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine.UI;
//using BeardedManStudios.Network;

public class PlayerController : NetworkBehaviour {
    public static PlayerController CurrentPlayer { get; protected set; }

    [SyncVar]
    public string PlayerID;

    public bool IsLoggedIn
    {
        get { return PlayerID != null; }
    }

    public GameObject partyPrefab;

    public GameObject Party { get; set; }

    #region Shared
    // ********************************
    // Client and Server shared methods
    // ********************************

    void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    void Start() {

    }

    void Update () {
        if (isServer)
            UpdateServer();
        else if (isClient)
            UpdateClient();
    }

    #endregion

    #region Client
    // *******************
    // Client only methods
    // *******************

    public override void OnStartLocalPlayer()
    {
        base.OnStartLocalPlayer();
        CurrentPlayer = this;
        GameManager.instance.currentPlayer = gameObject;
    }

    public override void OnStartClient()
    {
        base.OnStartClient();
    }

    [ClientCallback]
    void OnLevelWasLoaded(int level)
    {
        if (level == 1)
        {
            CmdEnterWorld();
        }
    }

    [Client]
    public void RequestLogin(string playerID)
    {
        CmdLogin(playerID);
    }

    [Client]
    public void RequestToggleTraveling()
    {
        CmdToggleTraveling();
    }

    [Client]
    private void UpdateClient()
    {

    }

    #endregion

    #region Server
    // *******************
    // Server only methods
    // *******************

    public override void OnStartServer()
    {
        base.OnStartServer();
    }
    
    [Server]
    private void UpdateServer()
    {
        
    }

    [Server]
    protected void SpawnParty()
    {
        var party = Instantiate(partyPrefab);
        var ppc = party.GetComponent<PlayerPartyController>();
        ppc.PlayerID = PlayerID;
        NetworkServer.Spawn(party);
        this.Party = party;
    }

    #endregion

    #region Commands
    // ***************
    // Server Commands
    // ***************

    [Command]
    protected void CmdLogin(string playerID)
    {
        this.PlayerID = playerID;
        RpcLoginSuccess();
    }

    [Command]
    protected void CmdEnterWorld()
    {
        SpawnParty();
    }

    [Command]
    protected void CmdToggleTraveling()
    {
        //stateMachine.Fire(PlayerTrigger.ToggleTraveling);
    }

    #endregion

    #region ClientRPCs
    // *****************
    // ClientRPC Methods
    // *****************

    [ClientRpc]
    protected void RpcLoginSuccess()
    {
        Application.LoadLevel("map");
    }

    [ClientRpc]
    protected void RpcLoginFailure()
    {

    }

    #endregion
}
