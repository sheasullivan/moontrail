﻿using UnityEngine;
using System.Collections;
using System;
using HoneyFramework;
using UnityEngine.UI;

public class StatusController : MonoBehaviour {
    public static StatusController instance;

    public GameObject MoveProgressField;

    private void Awake()
    {
        instance = this;
        gameObject.SetActive(false);
    }

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        UpdateMoveProgress();
	}

    public void TogglePanel()
    {
        gameObject.SetActive(!gameObject.activeSelf);
    }

    public void OpenPanel()
    {
        gameObject.SetActive(true);
    }

    public void ClosePanel()
    {
        gameObject.SetActive(false);
    }

    private void UpdateMoveProgress()
    {
        if (GameManager.instance.currentPlayer)
        {
            var pc = PlayerController.CurrentPlayer.Party.GetComponent<PlayerPartyController>();
            MoveProgressField.GetComponent<Text>().text = pc.MoveProgress.ToString();
        }
    }
}
