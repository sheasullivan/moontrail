package onthemoon.moonserver

import co.paralleluniverse.actors.ActorRef
import co.paralleluniverse.actors.ActorSpec
import co.paralleluniverse.actors.behaviors.Supervisor
import co.paralleluniverse.actors.behaviors.SupervisorActor
import java.util.concurrent.TimeUnit


class PlayerFactory(val supervisor: Supervisor) {
    public fun Create(name: String): ActorRef<Any?> {
        val actorID = "player-$name"
        return supervisor.addChild<ActorRef<Any?>, Any>(Supervisor.ChildSpec(actorID, Supervisor.ChildMode.PERMANENT, 3, 1, TimeUnit.SECONDS, 3, ActorSpec.of(Player::class.java, name)))
    }
}