﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using HoneyFramework;
using UnityEngine.UI;

public class LoginUIController : MonoBehaviour {
    public GameObject PlayerNameInput;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Login()
    {
        PlayerController.CurrentPlayer.RequestLogin(PlayerNameInput.GetComponent<Text>().text);
    }
}
