﻿using UnityEngine;
using System.Collections;
using System;
using HoneyFramework;
using Stateless;
using Pathfinding;
using System.Collections.Generic;
using UnityEngine.Networking;
//using BeardedManStudios.Network;

public class OtherPlayerController : NetworkBehaviour
{
    [SyncVar(hook = "OnHexPositionChanged")]
    public Vector3i HexPosition;

    [SyncVar]
    public float MoveProgress = 0;

    //[NetSync]
    //public string PlayerId { get; set; }

    private StateMachine<PlayerState, PlayerTrigger> stateMachine;
    private float speed = .1f;

    private enum PlayerState
    {
        Resting,
        Traveling
    }

    private enum PlayerTrigger
    {
        Travel,
        Rest,
        ToggleTraveling
    }


    #region Shared
    // ********************************
    // Client and Server shared methods
    // ********************************

    void Awake()
    {
        InitStateMachine();
    }

    void Start()
    {
        if (isLocalPlayer)
        {
            GameManager.instance.currentPlayer = gameObject;
        }
        transform.parent = World.instance.transform;
        transform.localPosition = Vector3.zero;
    }

    void Update()
    {
        if (isServer)
            UpdateServer();
        else if (isClient)
            UpdateClient();
    }

    protected void Rest()
    {
        stateMachine.Fire(PlayerTrigger.Rest);
    }

    protected void Travel()
    {
        stateMachine.Fire(PlayerTrigger.Travel);
    }

    protected void AnimateToPosition(Vector3i hexPosition)
    {
        var formation = GetComponent<Formation>();

        // Animate movement if distance is small, otherwise teleport
        if (HexCoordinates.HexDistance(formation.position, hexPosition) < 5)
        {
            formation.GoTo(hexPosition);
        }
        else
        {
            formation.TeleportTo(hexPosition);
        }
    }

    protected void UpdateFormation()
    {
        var formation = GetComponent<Formation>();
        AnimateToPosition(HexPosition);

        //TODO: Should be based on current status
        for (int i = 0; i < 5; i++)
        {
            formation.AddCharacter(CharacterActor.CreateCharacter("Characters/explorer", 2f));
        }
    }

    private void InitStateMachine()
    {
        stateMachine = new StateMachine<PlayerState, PlayerTrigger>(PlayerState.Resting);
        stateMachine.Configure(PlayerState.Resting)
            .OnEntry(t => StartResting())
            .OnExit(t => StopResting())
            .Permit(PlayerTrigger.Travel, PlayerState.Traveling)
            .Permit(PlayerTrigger.ToggleTraveling, PlayerState.Traveling);
        stateMachine.Configure(PlayerState.Traveling)
            .OnEntry(t => StartTraveling())
            .OnExit(t => StopTraveling())
            .Permit(PlayerTrigger.Rest, PlayerState.Resting)
            .Permit(PlayerTrigger.ToggleTraveling, PlayerState.Resting);
    }

    private void OnHexPositionChanged(Vector3i value)
    {
        AnimateToPosition(value);
    }

    private void StartResting()
    {

    }

    private void StopResting()
    {

    }

    private void StartTraveling()
    {

    }

    private void StopTraveling()
    {

    }

    #endregion

    #region Client
    // *******************
    // Client only methods
    // *******************

    public override void OnStartClient()
    {
        base.OnStartClient();
        UpdateFormation();
    }

    [Client]
    public void RequestToggleTraveling()
    {
        CmdToggleTraveling();
    }

    [Client]
    private void UpdateClient()
    {
        DoMovementClient();
    }

    [Client]
    private void DoMovementClient()
    {
        if (stateMachine.IsInState(PlayerState.Traveling))
        {
            MoveProgress += Time.deltaTime * speed;

            if (MoveProgress >= 1)
            {
                MoveProgress = 1;
            }
        }
    }

    #endregion

    #region Server
    // *******************
    // Server only methods
    // *******************

    public override void OnStartServer()
    {
        base.OnStartServer();
        HexPosition = GameManager.trailStartPosition;
    }

    [Server]
    private void UpdateServer()
    {
        DoMovementServer();
    }

    [Server]
    private void DoMovementServer()
    {
        if (stateMachine.IsInState(PlayerState.Traveling))
        {
            MoveProgress += Time.deltaTime * speed;

            if (MoveProgress >= 1)
            {
                MoveProgress = 0;
                AdvancePosition();
            }
        }
    }

    [Server]
    private void AdvancePosition()
    {
        GetComponent<Seeker>().StartPath(HexPosition, (Vector3)GameManager.trailEndPosition, MoveOneTile);
    }

    [Server]
    private void MoveOneTile(Path p)
    {
        //var newPosition = new Vector3(Mathf.Round(p.path[1].position.x / 1000), Mathf.Round(p.path[1].position.y / 1000), Mathf.Round(p.path[1].position.z / 1000));
        var newPosition = new Vector3i(p.path[1].position);
        HexPosition = newPosition;
    }

    #endregion

    #region Commands
    // ***************
    // Server Commands
    // ***************

    [Command]
    protected void CmdToggleTraveling()
    {
        stateMachine.Fire(PlayerTrigger.ToggleTraveling);
    }

    #endregion
}
